class CreateHospitals < ActiveRecord::Migration
  def change
    create_table :hospitals do |t|
     t.string :pname
     t.string :address
     t.float :age
     
      t.timestamps null: false
    end
  end
end
