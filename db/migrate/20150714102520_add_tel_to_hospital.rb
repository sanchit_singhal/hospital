class AddTelToHospital < ActiveRecord::Migration
  def change
    add_column :hospitals, :tel, :float
  end
end
